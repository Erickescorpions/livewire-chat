<div>
    <form wire:submit.prevent="sendMessage" class="w-full max-w-sm p-4">
        <div class="mb-5">
            <label for="username" class="block mb-2 text-sm font-medium text-gray-900">Nombre de usuario</label>
            <input wire:model.live="username" type="text" id="username" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="Ingresa tu nombre de usuario">
            @error('username') <span class="text-xs text-red-500">{{ $message }}</span> @enderror
        </div>

        <div class="mb-5">
            <label for="message" class="block mb-2 text-sm font-medium text-gray-900">Escribe tu mensaje</label>
            <textarea wire:model.live="message" id="message" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"> </textarea>
            @error('message') <span class="text-xs text-red-500">{{ $message }}</span> @enderror
        </div>

        <input type="submit" value="Enviar mensaje" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
    </form>


    @if($showAlert)
    <div id="alert" class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded" role="alert">
        <strong class="font-bold">Mensaje enviado</strong>
    </div>
    @endif

    @script
    <script>   
        $("#alert").hide();
    
        Livewire.on("messageSentAlert", () => {
            $("#alert").fadeIn("slow");

            setTimeout(() => {
                $("#alert").fadeOut("slow");
                $wire.set("showAlert", false);
            }, 3000);
        });  
    </script>
    @endscript
</div>
