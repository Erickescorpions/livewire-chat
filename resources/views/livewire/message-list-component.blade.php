<div>
    <h1>Lista de mensajes</h1>
    <ul>
        @foreach ($messages as $message)
            <li>
                <div>
                    <strong>{{ $message['username'] }}</strong>
                    <span>{{ $message['message'] }}</span>
                </div>
            </li>
        @endforeach
    </ul>

    {{-- @script
    <script>
        window.onload = function() {
            Echo.channel('chat-channel')
            .listen('.sendingMessage', (data) => {
                console.log(data);
                $wire.dispatch('newMessageSent', {
                    message: data
                });
            });
        }
    </script>
    @endscript --}}

</div>
