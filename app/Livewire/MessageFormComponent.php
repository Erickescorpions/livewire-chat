<?php

namespace App\Livewire;

use Livewire\Component;
use Livewire\Attributes\Validate;
use App\Events\SendingMessageEvent;
use App\Livewire\MessageListComponent;

class MessageFormComponent extends Component
{
    #[Validate('required|min:3')] 
    public $username;

    #[Validate('required|min:3')] 
    public $message;

    public $showAlert;

    public function mount()
    {
        $this->username = "";
        $this->message = "";
        $this->showAlert = false;
    }

    public function sendMessage()
    {
        $validated = $this->validate();

        $this->showAlert = true;
        $this->dispatch("messageSentAlert");
        // $this->dispatch("newMessageSent", [
        //     'username' => $this->username,
        //     'message' => $this->message
        // ]);

        event(new SendingMessageEvent($this->username, $this->message));
    }

    public function render()
    {
        return view('livewire.message-form-component');
    }
}
