<?php

namespace App\Livewire;

use Livewire\Component;
use Livewire\Attributes\On;

class MessageListComponent extends Component
{
    public array $messages;

    public function mount()
    {
        $this->messages = [];
    }

    #[On('newMessageSent')]
    #[On('echo:chat-channel,.sendingMessage')]
    public function addMessage($message) {
        array_push($this->messages, $message);
    }

    public function render()
    {
        return view('livewire.message-list-component');
    }
}
