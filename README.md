# Broadcasting y Livewire

Laravel Broadcasting es una característica de Laravel, que permite la transmisión de eventos en tiempo real a través de diferentes canales, lo que facilita la construcción de aplicaciones en tiempo real, como chats, notificaciones y actualizaciones en tiempo real de datos.

Utiliza eventos para representar acciones, escuchadores para manejar lógica, colas para ejecución en segundo plano, y transmisores para enviar eventos a canales como Pusher o Redis. Los canales pueden ser públicos o privados, y puedes elegir entre varios proveedores de transmisión. 

Vamos a realizar un evento para mandar mensajes en tiempo real, basado en la serie de videos de YouTube de `Laravel en Español` 

[Creación de un Chat en Tiempo Real (Laravel 7 + Livewire + Pusher)](https://www.youtube.com/playlist?list=PLqDpGDafa4MByRNKk2GPAppdLJh292jJH)

Para realizar este ejemplo usaremos como proveedor de transmisión Pusher, Pusher es un servicio en la nube que facilita la implementación de funcionalidades en tiempo real en aplicaciones web y móviles. Proporciona una infraestructura para la transmisión de datos en tiempo real a través de WebSockets. Cuando utilizas Laravel Broadcasting con Pusher, Laravel actúa como un puente que emite eventos a Pusher, y Pusher se encarga de transmitir esos eventos a los clientes suscritos a través de WebSockets. Esto facilita la construcción de aplicaciones en tiempo real de manera eficiente y escalable.

Sandbox permite un total de 100 conexiones concurrentes y 200 mil mensajes por dia. 

## Canales de Pusher

### Instalación

Para el lado del servidor es necesario instalar el paquete **`pusher/pusher-php-server`**

```bash
composer require pusher/pusher-php-serverv
```

Una vez instalado el paquete, es necesario realizar algunas configuraciones en el .env, para esto primero es necesario registrar una “aplicación” en pusher y obtener las credenciales, estas las vamos a colocar en las siguientes opciones:

```php
PUSHER_APP_ID=your-pusher-app-id
PUSHER_APP_KEY=your-pusher-key
PUSHER_APP_SECRET=your-pusher-secret
PUSHER_APP_CLUSTER=mt1

BROADCAST_DRIVER=pusher
```

Para el lado del cliente se puede instalar Laravel echo y el paquete pusher-js:

```bash
npm install --save-dev laravel-echo pusher-js
```

Adicionalmente, en el archivo `resources/js/bootstrap.js` vamos a des comentar la configuración existente para usar laravel echo, si esta configuración no existe nos podemos basar en el siguiente formato: 

```jsx
import Echo from 'laravel-echo';
import Pusher from 'pusher-js';
 
window.Pusher = Pusher;
 
window.Echo = new Echo({
    broadcaster: 'pusher',
    key: import.meta.env.VITE_PUSHER_APP_KEY,
    cluster: import.meta.env.VITE_PUSHER_APP_CLUSTER,
    forceTLS: true
});
```

Una vez realizado esto es necesario compilar los assets de la aplicación:

```bash
npm run build
```

### Definición de eventos

Laravel Broadcasting utiliza los eventos de Laravel, estos eventos se utilizan principalmente para desacoplar las funcionalidades en una aplicación, permitiendo que este evento tomo papel como un emisor para dar a conocer que una acción acaba de suceder. Comúnmente estos van acompañados de listeners para cachar el evento, pero para el uso de Broadcasting solo se utiliza el evento. 

- Para crear un evento se utiliza el siguiente comando:

```bash
php artisan make:event MiEvento
```

Esto creara un archivo en `app/Event/` 

- Es importante que la clase del evento extienda la clase `Illuminate\Contracts\Broadcasting\ShouldBroadcast` .
- Para este ejemplo crearemos un evento que contenga la informacion del mensaje.

```php
class SendingMessageEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $username;
    public $message;

    public function __construct($username, $message)
    {
        $this->username = $username;
        $this->message = $message;
    }

    public function broadcastOn()
    {
        return 'chat-channel';
    }

    public function broadcastAs()
    {
        return 'sendingMessage';
    }
}
```

Como podemos observar definimos el nombre de usuario y el mensaje como atributos del evento y estos los recibimos mediante el constructor del evento, también tenemos dos métodos, `broadcastOn` define el canal por el cual se va a mandar nuestro evento y `broadcastAs` define el nombre con el cual se va a emitir el evento. 

### Utilizar los eventos

```php
event(new SendingMessageEvent($this->username, $this->message));
```

### Escuchar eventos

Para poder escuchar los eventos se utiliza la siguiente sintaxis con JS

```jsx
@script
    <script>
        window.onload = function() {
            Echo.channel('chat-channel')
            .listen('.sendingMessage', (data) => {
                console.log(data);
                $wire.dispatch('newMessageSent', {
                    message: data
                });
            });
        }
    </script>
@endscript
```

Estamos utilizando livewire v3, por eso se esta utilizando la directiva @script y $wire. 

Tambien es importante mencionar que para recibir de manera correcta el evento es importante anteponer un “.” antes del nombre del evento para evitar que se le ponga un prefijo. 

Cachamos el evento de esta manera debido a que es un canal “publico”, pero si fuera  un canal privado lo cacharíamos de la siguiente manera: 

```jsx
Echo.private('canal')
    .listen(/* ... */)
    .listen(/* ... */)
    .listen(/* ... */);
```

Por otro lado, podemos evitarnos escuchar eventos con JS si estamos usando Livewire. 
Para esto utilizamos la sintaxis establecida por Livewire:

```php
#[On('echo:chat-channel,.sendingMessage')]
public function addMessage($message) {
    array_push($this->messages, $message);
}
```

Como podemos observar estamos usando la nueva sintaxis de Livewire v3 en donde utilizamos la clase On, sin embargo lo importante es la definición del evento. 

Para recibir estos eventos utilizamos la siguiente sintaxis: `echo:canal,nombre_evento`